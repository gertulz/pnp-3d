# PnP Problem

import math
import pandas as pd
import numpy as np
from scipy import linalg
#import plotly.graph_objects as go


def getP3D():
    # Get P3D
    P3D_0 = pd.DataFrame([[math.sqrt(3)/2, 1/2, 0],
                          [-1/2, math.sqrt(3)/2, 0],
                          [0, 0, 1]])
    P3D_1 = pd.DataFrame([[1, 0, 0],
                         [0, math.sqrt(3)/2, 1/2],
                         [0, -1/2, math.sqrt(3)/2]])

    #P3D = P3D_0.dot(P3D_1)
    P3D = P3D_1
    P3D[4] = [10, 20, 30]

    return P3D

def read_coords(filename):
    # Reading coordinates. Also add homogenious coordinates
    coords = pd.read_csv(filename)
    coords[coords.shape[1]-1] = 1

    return coords

def get_image_matrix(p_matrix, coordinates_matrix_local):
    # Calculating image matrix
    def multiply_matrices(df1, df2):
        # Columns names must coincide with the index of the other matrix for succesfull .dot
        return df1.dot(df2.T)

    for x in [p_matrix, coordinates_matrix_local]:
        x.columns = list(range(0, x.shape[1]))
        x = x.astype(float)

    return multiply_matrices(p_matrix, coordinates_matrix_local)

def add_images_to_coordinates(coordinates_matrix, image_matrix_local):
    # Adding the image of the points to the old matrix
    coordinates_matrix['image_x'] = image_matrix_local.T[0]/image_matrix_local.T[2]
    coordinates_matrix['image_y'] = image_matrix_local.T[1]/image_matrix_local.T[2]
    return coordinates_matrix

def get_A3D(coords_and_images):
    # Computing A to solve Ax = 0
    df = coords_and_images
    one_v = np.array([1]*coords_and_images.shape[0])
    zero_v = np.array([0]*coords_and_images.shape[0])
    minusone_v = np.array([-1]*coords_and_images.shape[0])
    
    first_row = pd.DataFrame([zero_v, zero_v, zero_v, zero_v,
                              -df['world_x'].values, -df['world_y'].values, -df['world_z'].values, minusone_v,
                              (df['world_x']*df['image_y']).values, (df['world_y']*df['image_y']).values, (df['world_z']*df['image_y']).values,
                              df['image_y'].values]).T

    second_row = pd.DataFrame([df['world_x'].values, df['world_y'].values, df['world_z'].values, one_v,
                               zero_v, zero_v, zero_v, zero_v,
                               (-df['image_x']*df['world_x']).values, (-df['image_x']*df['world_y']).values, (-df['image_x']*df['world_z']).values,
                               (-df['image_x']).values]).T

    return pd.concat([first_row, second_row])

def get_P_approx(A_matrix, verbose=False):
    # Singular Value descomposition. Taking the vector with the lowest value of the diagonal matrix
    U, D, V = np.linalg.svd(A_matrix)
    p_stack = V[D.argmin()]

    # The dimension of our P is 3x4
    p_approx = p_stack.reshape((3, 4))
    p_approx = pd.DataFrame(data=p_approx)
    last_column = p_approx.shape[1]-1

    # Calculate rotation and append translation already calculated.
    p_aux = p_approx.drop(last_column, axis=1)
    
    det_p_aux = np.linalg.det(p_aux)
    factor = abs(det_p_aux) ** (1. / 3)
    
    p_aux = np.divide(p_aux, factor)
    
    # Take matrix from SO(3)
    p_aux = p_aux.dot(linalg.inv(linalg.sqrtm(p_aux.T.dot(p_aux))))
    
    p_aux = pd.concat([p_aux, np.divide(p_approx[last_column], factor)], axis=1)

    # Multiply or divide every value by -1.
    if (det_p_aux < 0):
        p_aux = np.divide(p_aux, -1)

    return p_aux


if __name__ == '__main__':

    P3D = getP3D()
    
    # Direct problem.
    coordinates3D = read_coords('coordinates.txt')
    image_matrix3D = get_image_matrix(P3D, coordinates3D.copy())
    coordinates_with_image3D = add_images_to_coordinates(coordinates3D.copy(), image_matrix3D)

    # Inverse problem.
    A3D = get_A3D(coordinates_with_image3D)
    P_approx = get_P_approx(A3D, False)

    # Result of the inverse problem.
    print('P3D:')
    print(P3D)
    print('P_approx:')
    print(P_approx)
